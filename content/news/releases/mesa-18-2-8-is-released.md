---
title:    "Mesa 18.2.8 is released"
date:     2018-12-27 00:00:00
category: releases
tags:     []
summary:  "[Mesa 18.2.8](https://docs.mesa3d.org/relnotes/18.2.8.html) is released. This is a bug-fix
release."
---
[Mesa 18.2.8](https://docs.mesa3d.org/relnotes/18.2.8.html) is released. This is a bug-fix
release.

{{< alert type="info" title="Note" >}}
It is anticipated that 18.2.8 will be the final release in the
18.2 series. Users of 18.2 are encouraged to migrate to the 18.3 series
in order to obtain future fixes.
{{< /alert >}}
