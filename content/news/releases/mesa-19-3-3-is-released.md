---
title:    "Mesa 19.3.3 is released"
date:     2020-01-28 00:00:00
category: releases
tags:     []
---
[Mesa 19.3.3](https://docs.mesa3d.org/relnotes/19.3.3.html) is released. This is a bug fix
release.
