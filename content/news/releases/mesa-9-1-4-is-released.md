---
title:    "Mesa 9.1.4 is released"
date:     2013-07-01 00:00:00
category: releases
tags:     []
---
[Mesa 9.1.4](https://docs.mesa3d.org/relnotes/9.1.4.html) is released. This is a bug fix
release.
