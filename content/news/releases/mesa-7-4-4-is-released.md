---
title:    "Mesa 7.4.4 is released"
date:     2009-06-23 00:00:00
category: releases
tags:     []
---
[Mesa 7.4.4](https://docs.mesa3d.org/relnotes/7.4.4.html) is released. This is a stable release
that fixes a regression in the i915/i965 drivers that slipped into the
7.4.3 release.
