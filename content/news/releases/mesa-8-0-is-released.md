---
title:    "Mesa 8.0 is released"
date:     2012-02-09 00:00:00
category: releases
tags:     []
---
[Mesa 8.0](https://docs.mesa3d.org/relnotes/8.0.html) is released. This is the first version of
Mesa to support OpenGL 3.0 and GLSL 1.30 (with the i965 driver). See the
release notes for more information about the release.
