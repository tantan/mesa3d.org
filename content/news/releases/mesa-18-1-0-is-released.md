---
title:    "Mesa 18.1.0 is released"
date:     2018-05-18 00:00:00
category: releases
tags:     []
---
[Mesa 18.1.0](https://docs.mesa3d.org/relnotes/18.1.0.html) is released. This is a new
development release. See the release notes for more information about
the release.
