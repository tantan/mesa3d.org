---
title:    "Mesa 20.3.2 is released"
date:     2020-12-30 20:21:51
category: releases
tags:     []
---
[Mesa 20.3.2](https://docs.mesa3d.org/relnotes/20.3.2.html) is released. This is a bug fix release.
