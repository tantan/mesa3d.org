---
title:    "Mesa 10.6.6 is released"
date:     2015-09-04 00:00:00
category: releases
tags:     []
---
[Mesa 10.6.6](https://docs.mesa3d.org/relnotes/10.6.6.html) is released. This is a bug-fix
release.
