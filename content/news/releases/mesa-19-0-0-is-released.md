---
title:    "Mesa 19.0.0 is released"
date:     2019-03-13 00:00:00
category: releases
tags:     []
---
[Mesa 19.0.0](https://docs.mesa3d.org/relnotes/19.0.0.html) is released. This is a new
development release. See the release notes for more information about
this release
