---
title:    "Mesa 7.1 is released"
date:     2008-08-26 00:00:00
category: releases
tags:     []
---
[Mesa 7.1](https://docs.mesa3d.org/relnotes/7.1.html) is released. This is a new development
release. It should be relatively stable, but those especially concerned
about stability should wait for the 7.2 release or use Mesa 7.0.4 (the
previous stable release).
