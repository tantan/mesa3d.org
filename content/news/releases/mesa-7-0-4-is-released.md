---
title:    "Mesa 7.0.4 is released"
date:     2008-08-16 00:00:00
category: releases
tags:     []
---
[Mesa 7.0.4](https://docs.mesa3d.org/relnotes/7.0.4.html) is released. This is a bug-fix
release.
