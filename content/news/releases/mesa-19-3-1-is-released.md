---
title:    "Mesa 19.3.1 is released"
date:     2019-12-18 00:00:00
category: releases
tags:     []
---
[Mesa 19.3.1](https://docs.mesa3d.org/relnotes/19.3.1.html) is released. This is a bug fix
release.
