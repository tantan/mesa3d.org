---
title:    "Mesa 10.4 is released"
date:     2014-12-14 00:00:00
category: releases
tags:     []
---
[Mesa 10.4](https://docs.mesa3d.org/relnotes/10.4.html) is released. This is a new development
release. See the release notes for more information about the release.
