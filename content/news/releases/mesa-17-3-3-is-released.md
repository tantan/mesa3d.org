---
title:    "Mesa 17.3.3 is released"
date:     2018-01-18 00:00:00
category: releases
tags:     []
---
[Mesa 17.3.3](https://docs.mesa3d.org/relnotes/17.3.3.html) is released. This is a bug-fix
release.
