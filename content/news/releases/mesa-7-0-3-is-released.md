---
title:    "Mesa 7.0.3 is released"
date:     2008-04-04 00:00:00
category: releases
tags:     []
---
[Mesa 7.0.3](https://docs.mesa3d.org/relnotes/7.0.3.html) is released. This is a bug-fix
release.
