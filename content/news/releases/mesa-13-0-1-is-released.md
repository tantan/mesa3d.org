---
title:    "Mesa 13.0.1 is released"
date:     2016-11-14 00:00:00
category: releases
tags:     []
---
[Mesa 13.0.1](https://docs.mesa3d.org/relnotes/13.0.1.html) is released. This is a bug-fix
release.
