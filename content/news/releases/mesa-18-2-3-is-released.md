---
title:    "Mesa 18.2.3 is released"
date:     2018-10-19 00:00:00
category: releases
tags:     []
---
[Mesa 18.2.3](https://docs.mesa3d.org/relnotes/18.2.3.html) is released. This is a bug-fix
release.
