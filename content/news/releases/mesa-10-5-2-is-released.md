---
title:    "Mesa 10.5.2 is released"
date:     2015-03-28 00:00:00
category: releases
tags:     []
---
[Mesa 10.5.2](https://docs.mesa3d.org/relnotes/10.5.2.html) is released. This is a bug-fix
release.
