---
title:    "Mesa 17.0.4 is released"
date:     2017-04-17 00:00:00
category: releases
tags:     []
---
[Mesa 17.0.4](https://docs.mesa3d.org/relnotes/17.0.4.html) is released. This is a bug-fix
release.
