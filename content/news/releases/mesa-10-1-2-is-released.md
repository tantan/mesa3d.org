---
title:    "Mesa 10.1.2 is released"
date:     2014-05-05 00:00:00
category: releases
tags:     []
---
[Mesa 10.1.2](https://docs.mesa3d.org/relnotes/10.1.2.html) is released. This is a bug-fix
release.
