---
title:    "Mesa 11.0.8 is released"
date:     2015-12-21 00:00:00
category: releases
tags:     []
---
[Mesa 11.0.8](https://docs.mesa3d.org/relnotes/11.0.8.html) is released. This is a bug-fix
release.
