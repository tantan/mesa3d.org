---
title:    "Mesa 20.0.1 is released"
date:     2020-03-05 00:00:00
category: releases
tags:     []
---
[Mesa 20.0.1](https://docs.mesa3d.org/relnotes/20.0.1.html) is released. This is a bug fix
release.
