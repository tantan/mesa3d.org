---
title:    "Mesa 20.1.9 is released"
date:     2020-09-30 20:31:31
category: releases
tags:     []
---
[Mesa 20.1.9](https://docs.mesa3d.org/relnotes/20.1.9.html) is released. This is a bug fix release.
