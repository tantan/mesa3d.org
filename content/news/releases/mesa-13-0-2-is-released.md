---
title:    "Mesa 13.0.2 is released"
date:     2016-11-28 00:00:00
category: releases
tags:     []
---
[Mesa 13.0.2](https://docs.mesa3d.org/relnotes/13.0.2.html) is released. This is a bug-fix
release.
