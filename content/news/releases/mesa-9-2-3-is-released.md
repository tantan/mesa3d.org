---
title:    "Mesa 9.2.3 is released"
date:     2013-11-13 00:00:00
category: releases
tags:     []
---
[Mesa 9.2.3](https://docs.mesa3d.org/relnotes/9.2.3.html) is released. This is a bug fix
release.
