---
title:    "Mesa 6.4 has been released"
date:     2005-10-24 00:00:00
category: releases
tags:     []
---
[Mesa 6.4](https://docs.mesa3d.org/relnotes/6.4.html) has been released. This is stable,
bug-fix release.
