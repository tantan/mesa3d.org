---
title:    "Mesa 18.3.6 is released"
date:     2019-04-05 00:00:00
category: releases
tags:     []
summary:  "[Mesa 18.3.6](https://docs.mesa3d.org/relnotes/18.3.6.html) is released. This is a bug-fix
release."
---
[Mesa 18.3.6](https://docs.mesa3d.org/relnotes/18.3.6.html) is released. This is a bug-fix
release.

{{< alert type="info" title="Note" >}}
It is anticipated that 18.3.6 will be the final release in the
18.3 series. Users of 18.3 are encouraged to migrate to the 19.0 series
in order to obtain future fixes.
{{< /alert >}}
