---
title:    "May 24, 1999"
date:     1999-05-24 00:00:00
category: misc
tags:     []
---
Beta 2 of Mesa 3.1 has been make available at
<ftp://ftp.mesa3d.org/mesa/beta/>. If you are into the quake scene, you
may want to try this out, as it contains some optimizations specifically
in the Q3A rendering path.
