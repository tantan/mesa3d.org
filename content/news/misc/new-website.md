---
title:    "New website launched"
date:     2020-07-14 15:08:00
category: misc
---
Welcome to the new and improved mesa3d.org!

A few weeks ago, we silently rolled out a new website for Mesa, and now it's
finally time to spread the word.

<!--more-->

The old site was getting hard to maintain by today's standards, and with the
arrival of mobile devices on the internet, things were overdue for a refresh.

Don't worry, no content has been lost. The site has been split in two, with a
modern new homepage and news-archive built with Hugo and Bootstrap. The
content from the old site was mostly documentation and has been migrated to
<http://docs.mesa3d.org/>, where it has been cleaned up and modernized using
Sphinx.

Extra emphasis has been put on ease of contributions to both the documentation
website and the new homepage, to make sure everyone knows how to contribute any
improvement. If it's still unclear, feel free to [ask for help in our website
repository](https://gitlab.freedesktop.org/mesa/mesa3d.org/-/issues).

You can read more about the new website [here]({{< ref "/website" >}}).
