---
title:    "December 17, 1999"
date:     1999-12-17 00:00:00
category: misc
tags:     []
---
A Slashdot interview with Brian about Mesa (questions submitted by
Slashdot readers) can be found at
<https://slashdot.org/interviews/99/12/17/0927212.shtml>.
